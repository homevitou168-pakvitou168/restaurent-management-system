<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## System Feature

- Super User
    - All Admin role + User Management
- Admin 
    - Dashboard for showing total sale info, sale info by category, period and top 20 -items
    - Manage Table
    - Manage Customer
    - Manage Item ( it will be recipe of product)
    - Manage Item Category
    - Manage Product
    - Manage Product Category
    - Recipe
    - Report
        - Stock Balance
        - Daily Summary
        - Sale History
        - Sale Detail
        - Deleted Item
        - Sale Stock
        - Sale Discount
        - Sale Graph Report
- Cashier
    - Click Product to Order
    - Order by Product Code
    - Filter Product
    - Move Table
    - Open Product ( if no product for selection, we can type)
    - Split Bill
    - Update Product Description, Qty, and Unitprice
    - Discount
    - VIP Customer

## Technologie that  I will use in system
- laravel 5.7
- laravelCollective (for making form)
- Bootstrap 4.*
- jquery
- etc 
